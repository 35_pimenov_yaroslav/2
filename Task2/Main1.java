import java.io.*;

public class Main1 {

    public static void main1(String[] args) throws IOException, ClassNotFoundException {
        // Створення об'єкта
        var person = new Person("John", 30, "password");

        // Серіалізація об'єкта
        FileOutputStream fileOut = new FileOutputStream("person.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(person);
        out.close();
        fileOut.close();
        System.out.println("Person serialized");

        // Десеріалізація об'єкта
        FileInputStream fileIn = new FileInputStream("person.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        Person deserializedPerson = (Person) in.readObject();
        in.close();
        fileIn.close();
        System.out.println("Person deserialized");
        System.out.println("Name: " + deserializedPerson.getName());
        System.out.println("Age: " + deserializedPerson.getAge());
        System.out.println("Password: " + deserializedPerson.getPassword()); // Значення поля password буде null
    }
}

class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private int age;
    private transient String password;

    public Person(String name, int age, String password) {
        this.name = name;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
