import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        
        // Введення висоти та довжини сторони основи
        System.out.print("Введіть висоту трикутника: ");
        double height = scanner.nextDouble();
        System.out.print("Введіть довжину сторони основи трикутника та прямокутника: ");
        double base = scanner.nextDouble();
        
        // Обчислення периметрів трикутника та прямокутника
        double trianglePerimeter = 2 * Math.sqrt((base / 2) * (base / 2) + height * height) + base;
        double rectanglePerimeter = 2 * (base + height);
        double sumPerimeter = trianglePerimeter + rectanglePerimeter;
        
        // Перетворення суми периметрів в двійкове представлення та підрахунок кількості 1
        int binary = (int) sumPerimeter;
        int countOnes = Integer.bitCount(binary);
        
        // Виведення результату
        System.out.println("Сума периметрів: " + sumPerimeter);
        System.out.println("Кількість 1 в двійковому представленні: " + countOnes);
        
        scanner.close();
    }
}
