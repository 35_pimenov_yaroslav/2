import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Клас для тестування коректності результатів обчислень та серіалізації/десеріалізації.
 */
public class TestClass {
    
    /**
     * Метод для перевірки коректності результату обчислення.
     * @param expected Очікуване значення.
     * @param actual Отримане значення.
     * @throws AssertionError Якщо отримане значення не дорівнює очікуваному.
     */
    public static void assertEquals(Object expected, Object actual) throws AssertionError {
        if (!expected.equals(actual)) {
            throw new AssertionError("Expected: " + expected + ", but was: " + actual);
        }
    }
    
    /**
     * Метод для перевірки коректності результату серіалізації та десеріалізації об'єкта.
     * @param object Об'єкт, який потрібно серіалізувати та десеріалізувати.
     * @throws IOException Якщо виникла помилка вводу/виводу.
     * @throws ClassNotFoundException Якщо клас не було знайдено.
     * @throws AssertionError Якщо серіалізований та десеріалізований об'єкти не є ідентичними.
     */
    public static void assertSerialization(Object object) throws IOException, ClassNotFoundException, AssertionError {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.close();
        
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        Object deserializedObject = objectInputStream.readObject();
        objectInputStream.close();
        
        if (!object.equals(deserializedObject)) {
            throw new AssertionError("Serialized and deserialized objects are not equal.");
        }
    }
}