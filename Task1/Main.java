import java.io.*;

/**
 *
 * @author User
 */
public class Main {
    public static void main(String[] args) {
        // Створення екземпляру класу Calculation
        Calculation calculation = new Calculation();

        // Серіалізація об'єкту класу Calculation у файл
        try {
            FileOutputStream fileOut = new FileOutputStream("calculation.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(calculation);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in calculation.ser");
        } catch (IOException i) {
        }

        // Десеріалізація об'єкту класу Calculation з файлу
        Calculation deserializedCalculation = null;
        try {
            FileInputStream fileIn = new FileInputStream("calculation.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            deserializedCalculation = (Calculation) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
        } catch (ClassNotFoundException c){
            System.out.println("Calculation class not found");
        }
    }
}
    